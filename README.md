# Gestion des cotisations du café

Utilise une base de données SQLite3 pour gérer les utilisateurs, l'argent versé et dépensé.

## liste des utilisateur
```bash
python3 gestion_cafe.py
```
Liste les utilisateurs dans la base, leur solde, s'ils sont actif ou pas.

```bash
python3 gestion_cafe.py --help
```
Donne le liste des paramètres possibles

## crontab

A lancer une fois par mois:
```bash
python3 gestion_cafe.py --cotisation
```
Aucune sortie, permet de retiré la cotisation définie pour chaque consommateur de café et d'envoyer un mail à chacun d'entre eux pour les informer de leur solde.
Envoi aussi un mail récapitulatif à l'administrateur.

### Ajouter
```bash
python3 gestion_cafe.py --ajouter
```
Permet d'ajouter un consommateur de café, qui par défaut est actif.

### Mofifier
```bash
python3 gestion_cafe.py --modifier
```
Permet de modifier un utilisateur, et aussi ajouter de l'argent pour ce consommateur afin de créditer son solde.

### Transaction
```bash
python3 gestion_cafe.py --transaction
```
Permet d'ajouter une transaction qui n'est pas lié à un consommateur, comme par exemple l'achat de café. Ou encore un solde initial qui n'appartient pas à un consommateur en particulier (comme les relicats laissés par les anciens consommateurs). Ne pas oublier le "-" pour les débits. 

