import sys
from pathlib import Path
import sqlite3
import argparse
import smtplib
from email.mime.text import MIMEText
from datetime import date

# configuration globale
database = Path.home().joinpath(".cafe.db")
admin_mail = "frederic.royer@obspm.fr"

class Consommateur:

    def __init__(self, db, row) -> None:
        self._db = db
        self._id = row["id"]
        self._prenom = row["prenom"]
        self._nom = row["nom"]
        self._mail = row["mail"]
        self._cotisation = row["cotisation"]
        self._actif = row["actif"]

    @staticmethod
    def newConsumer(db):
        print("Ajout d'un consommateur de café:")
        prenom = input(" - Prénom: ")
        nom = input(" - Nom: ")
        mail = input(" - mail: ")
        cotisation = input(" - cotisation mensuelle (en €): ")

        cur = db.cursor()
        result = cur.execute("SELECT COUNT(*) as count FROM consommateurs WHERE nom=? AND prenom=? COLLATE NOCASE", [nom, prenom])
        if result.fetchone()["count"] > 0:
            print("[ECHEC] Le consommateur", prenom, nom, "existe déjà dans la base")
            sys.exit(1)

        cur.execute("INSERT INTO consommateurs(nom, prenom, mail, cotisation, actif) VALUES (?,?,?,?,1)", [nom, prenom, mail, cotisation])
        db.commit()
        row = cur.execute("SELECT * FROM consommateurs WHERE nom=? AND prenom=? COLLATE NOCASE", [nom, prenom]).fetchone()
        print("[SUCCES]", prenom, nom, "ajouté avec succès")
        return Consommateur(db, row)

    @staticmethod
    def soldeCaisse(db):
        cur = db.cursor()
        return cur.execute("SELECT ROUND(SUM(montant),2) AS solde FROM transactions WHERE virtuel = 0").fetchone()["solde"]

    def solde(self):
        cur = self._db.cursor()
        return cur.execute("SELECT ROUND(SUM(montant),2) AS solde FROM transactions WHERE id=?", [str(self._id),]).fetchone()["solde"]

    def cotisation(self):
        self.newTransaction(1, -self._cotisation, "Cotisation mensuelle")
        solde = self.solde()
        msg_content = """<p>Bonjour {prenom},</p>
                         <p>Ce mail personnel, que vous recevrez chaque mois, vous informera de l'état de votre compte.</p>
                         <p>Le solde de votre participation au café est de <strong>{solde}€</strong> aujourd'hui.</p>
                         <p>Votre cotisation est actuellement de {cotisation}€/mois.</p>
                         <p>Cordialement.</p>""".format(prenom=self._prenom, solde=solde, cotisation=self._cotisation)
        to = self._mail
        subject = "[Cotisation café] Solde : {solde}€".format(solde=solde)
        envoyer(to, subject, msg_content)

        return solde

    def newTransaction(self, virtuel, montant, commentaire):
        cur = self._db.cursor()
        cur.execute("INSERT INTO transactions(id, virtuel, commentaire, montant) VALUES(?, ?, ?, ?)", [self._id, virtuel, commentaire, montant])
        self._db.commit()

    def setPrenom(self, prenom):
        if len(prenom) == 0:
            print("Prénom", self._prenom, "non modifié")
            return
        cur = self._db.cursor()
        cur.execute("UPDATE consommateurs SET prenom=? WHERE id=?", [prenom, self._id])
        self._db.commit()
        self._prenom = prenom

    def setNom(self, nom):
        if len(nom) == 0:
            print("Nom", self._nom, "non modifié")
            return
        cur = self._db.cursor()
        cur.execute("UPDATE consommateurs SET nom=? WHERE id=?", [nom, self._id])
        self._db.commit()
        self._nom = nom

    def setMail(self, mail):
        if len(mail) == 0:
            print("Mail", self._mail, "non modifié")
            return
        cur = self._db.cursor()
        cur.execute("UPDATE consommateurs SET mail=? WHERE id=?", [mail, self._id])
        self._db.commit()
        self._mail = mail

    def setCotisation(self, cotisation):
        if len(cotisation) == 0:
            print("Cotisation", self._cotisation, "non modifié")
            return
        cur = self._db.cursor()
        cur.execute("UPDATE consommateurs SET cotisation=? WHERE id=?", [cotisation, self._id])
        self._db.commit()
        self._cotisation = float(cotisation)

    def switchActif(self):
        cur = self._db.cursor()
        if self._actif == 1:
            self._actif = 0
        else:
            self._actif = 1
        cur.execute("UPDATE consommateurs SET actif=? WHERE id=?", [self._actif, self._id])
        self._db.commit()
        print("Maintenant", "actif" if self._actif == 1 else "inactif")

    def __str__(self):
        return "{} - {} {} ({}) {}€/mois est {}, solde {}€".format(self._id, self._prenom, self._nom, 
            self._mail, self._cotisation, "actif" if self._actif == 1 else "inactif", self.solde())


def init():
    db = sqlite3.connect(database)
    db.row_factory = sqlite3.Row
    return db


def initdb(db):
    cur = db.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS consommateurs (
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    prenom TEXT NOT NULL, 
                    nom TEXT NOT NULL, 
                    mail TEXT NOT NULL, 
                    cotisation REAL NOT NULL, 
                    actif INTEGER NOT NULL)""")
    cur.execute("""CREATE TABLE IF NOT EXISTS transactions (
                    id INTEGER NOT NULL,
                    virtuel BOOLEAN NOT NULL,
                    commentaire TEXT,
                    date TEXT DEFAULT CURRENT_TIMESTAMP, 
                    montant REAL NOT NULL)""")
    cur.execute("""CREATE INDEX IF NOT EXISTS idtransac on transactions(id)""")


def envoyer(to, subject, msg_content):
    server = smtplib.SMTP('localhost:25')
    message = MIMEText(msg_content, 'html')
    message['From'] = admin_mail
    message['To'] = to
    message['Subject'] = subject
    server.sendmail(admin_mail, to, message.as_string())
    server.quit()


def loadConsommateurs(db):
    cur = db.cursor()
    consommateurs = dict()
    for row in cur.execute('SELECT * FROM consommateurs'):
        consommateurs[str(row["id"])] = Consommateur(db, row)
    return consommateurs


def ajouterConsommateur(db):
    c = Consommateur.newConsumer(db)
    solde_initial = input("Solde initial (en €): ")
    c.newTransaction(1, solde_initial, "Solde initial")


def modifierConsommateur(db):
    consommateurs = loadConsommateurs(db)
    print("Liste des consommateurs:")
    for c in consommateurs.values():
        print(c)
    id = input("Numéro du consommateur à modifier: ")

    if id in consommateurs:
        c = consommateurs[id]
        fin = False
        while(not fin):
            print("Modifier :")
            print(" 1 - prénom")
            print(" 2 - nom")
            print(" 3 - mail")
            print(" 4 - cotisation mensuelle")
            print(" 5 - switch actif/inactif")
            print(" 6 - ajout d'argent")
            print(" 7 - Fin des modifications")
            r = input("Choix: ")

            if r == "1":
                prenom = input("Nouveau prénom: ")
                c.setPrenom(prenom)
            elif r == "2":
                nom = input("Nouveau nom: ")
                c.setNom(nom)
            elif r == "3":
                mail = input("Nouveau mail: ")
                c.setMail(mail)
            elif r == "4":
                cotisation = input("Nouvelle cotisation (appliqué à partir du prochain prélèvement): ")
                c.setCotisation(cotisation)
            elif r == "5":
                c.switchActif()
            elif r == "6":
                ajouter = input("Montant à ajouter (en €): ")
                c.newTransaction(0, ajouter, "cotisation")
            elif r == "7":
                fin = True
            else:
                print("Choix non reconnu")


def transaction(db):
    print("Ajout d'une transaction non lié à un utilisateur (achat, solde de la caisse ...)")
    montant = input("Montant de la transaction (en €): ")
    commentaire = input("Commentaire: ")
    cur = db.cursor()
    cur.execute("INSERT INTO transactions(id, virtuel, commentaire, montant) VALUES(?, 0, ?, ?)", [0, commentaire, montant])
    db.commit()


def cotisation(db):
    consommateurs = loadConsommateurs(db)
    today = date.today()
    subject="[Cotisation café] Bilan {mois}/{annee}".format(mois=today.month, annee=today.year)
    message_content="<p>bilan :<p><ul>"
    balance = 0
    somme_retard = 0
    for c in consommateurs.values():
        if c._actif == 1:
            solde = c.cotisation()
            message_content += "<li>" + c._prenom + " " + c._nom + " : " + str(solde) + "€</li>"
            balance += solde
            if solde < 0:
                somme_retard+=solde
    message_content += "</ul><p>Montant restant dans la caisse: " + str(Consommateur.soldeCaisse(db)) + "€</p>"
    message_content += "<p>Balance des cotisations des consommateurs: " + str(round(balance, 2)) + "€</p>"
    message_content += "<p>Somme des retards de cotisation: " + str(round(somme_retard,2)) + "€</p>"
    envoyer(admin_mail, subject, message_content)

def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--cotisation", action='store_true', help="Envoi un mail à chaque utilisateur pour l'informer de son solde et prélève la cotisation")
    group.add_argument("--ajouter", action='store_true', help="Permet d'ajouter un consommateur de café")
    group.add_argument("--modifier", action='store_true', help="Permet de modifier un consommateur de café")
    group.add_argument("--transaction", action='store_true', help="Transactions concernant le café")
    args = parser.parse_args()

    db = init()
    initdb(db)

    if args.cotisation:
        cotisation(db)
    elif args.ajouter:
        ajouterConsommateur(db)
    elif args.modifier:
        modifierConsommateur(db)
    elif args.transaction:
        transaction(db)
    else:
        consommateurs = loadConsommateurs(db)
        balance = 0
        somme_retard = 0
        somme_cotisation = 0
        for c in consommateurs.values():
            solde = c.solde()
            print(c)
            balance += solde
            if solde < 0:
                somme_retard+=solde
            if c._actif == 1:
                somme_cotisation += c._cotisation
        print("Montant restant dans la caisse:", Consommateur.soldeCaisse(db), "€")
        print("Balance des cotisations des consommateurs:", round(balance, 2), "€")
        print("Somme des retards de cotisation:", round(somme_retard, 2), "€")
        print("Somme des cotisations dues par l'ensemble des consommateurs:", abs(somme_cotisation), "€/mois")

    db.close()

if __name__ == "__main__":
    main()
